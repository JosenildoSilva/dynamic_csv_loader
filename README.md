# DYNAMIC_CSV_LOADER

# About that sample Ruby/Core App

 
Find a sample of each format in spec/fixtures

Combine the entries from the different formats

Sort the entries by some entry, for example: <first_name>

Format each entry to (first_name), (city), (birth_date M/D/YYYY)


Instructions

1) Clone the repository and run bundle install

2) Run bundle exec rspec

Verify that is one generic solution with dynamic field loading
that is possible adapt to other needs for other types of patterns.