class PeopleGenerator
  attr_accessor :source, :result, :curr_row

  def initialize(data_loader) 
    @source = data_loader.data

    @curr_row = Row.new
    
    self.dynamic_methods

    @result = Array.new

    self.extract_data
  end

  # dynamic define methods on Row class by csv headers
  def dynamic_methods
    columns.each do |column|      
      @curr_row.instance_eval do
        define_singleton_method(column) do
          self.row[column] 
        end
      end
    end
  end

  def columns; raise '#todo'; end

  # extract data from source csv and load row objects on result
  def extract_data    
    @source.each do |row|      
      @curr_row.row = row        
      @result << columns.map do |c|
        @curr_row.send(c)
      end     
    end      
  end

  # represent row data on csv
  class Row
    attr_accessor :row    
  end
end