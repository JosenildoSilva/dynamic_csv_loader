require_relative 'data_loader.rb'
require_relative 'people_data_generator.rb'

class PeopleController
  def initialize(params)
    @params = params
  end

  def normalize
    inputs = Array.new
    inputs << @params[:dollar_format]
    inputs << @params[:percent_format]
    order = @params['order']

    data_loader = DataLoader.new(inputs)
        
    people_data = PeopleDataGenerator.new(data_loader)
    
    x = (0 if order==:first_name) || (1 if order==:city) || (2 if order==:birthdate) || 0
    people_data.result.sort_by{|e| e[x]}.map{|x| x.join(", ")}        
  end

  private

  attr_reader :params
end