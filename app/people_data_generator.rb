require_relative 'people_generator.rb'

class PeopleDataGenerator < PeopleGenerator
   def columns ; %w(first_name city birthdate) ; end
end