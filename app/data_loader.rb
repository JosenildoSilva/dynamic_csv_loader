require 'csv'

class DataLoader
    attr_accessor :data, :inputs

    def initialize(inputs)
        @inputs = inputs 
        self.load_data
    end

    def load_data
        @data = Array.new          

        @inputs.each do |input|
            colsep = input.split[1]
            CSV.parse(
                input,                 
                headers:true, 
                force_quotes:true,
                row_sep:"\n",
                col_sep:colsep, 
                strip:true, 
                converters: [
                    ->(v) { Date.parse(v).strftime('%-m/%-d/%Y') rescue v },
                    ->(v) { v = ("Los Angeles" if v=="LA")||("New York City" if v=="NYC")||v rescue v }
                ]
            ).each do |row|
                @data << row.to_h
            end
        end        
    end
end