require 'spec_helper'
 
RSpec.describe 'App Load Data Test' do
  describe 'load pure data for generators' do
    let(:inputs) do
      [
        File.read('spec/fixtures/people_by_dollar.txt'),
        File.read('spec/fixtures/people_by_percent.txt'),        
      ]
    end
    let(:data_loader) { DataLoader.new(inputs) }

    it 'parses input files and outputs normalized data' do 
      
      expect(data_loader.data).to eq [
          {"city"=>"Los Angeles", "birthdate"=>"4/30/1974", "last_name"=>"Nolan", "first_name"=>"Rhiannon"}, 
          {"city"=>"New York City", "birthdate"=>"1/5/1962", "last_name"=>"Bruen", "first_name"=>"Rigoberto"}, 
          {"first_name"=>"Mckayla", "city"=>"Atlanta", "birthdate"=>"5/29/1986"}, 
          {"first_name"=>"Elliot", "city"=>"New York City", "birthdate"=>"5/4/1947"}
        ]
    end
  end
end