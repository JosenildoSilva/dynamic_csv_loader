require 'spec_helper'
 
RSpec.describe 'App Extract Unnormalized Data' do
  describe 'data extraction before normalization' do
    let(:input) do
        [
        File.read('spec/fixtures/people_by_dollar.txt'),
        File.read('spec/fixtures/people_by_percent.txt'),       
        ]
    end      
    let(:data_loader) { DataLoader.new(input) }
    
    let(:people_data_generator) { PeopleDataGenerator.new(data_loader) }

    it 'extract data and outputs unnormalized data' do    
            
      expect(people_data_generator.result).to eq [
          ["Rhiannon", "Los Angeles", "4/30/1974"], 
          ["Rigoberto", "New York City", "1/5/1962"], 
          ["Mckayla", "Atlanta", "5/29/1986"], 
          ["Elliot", "New York City", "5/4/1947"]
        ]
    end
  end
end